#Getting SSH keys
resource "hcloud_ssh_key" "ssh_key" {
  name        = var.ssh_key_name
  public_key  = file(var.ssh_public_cert)
}

#Random token share among k3s cluster components(master/nodes)
resource "random_string" "k3s_cluster_token" {
  length  = 48
  special = false
}

# Create a new server
resource "hcloud_server" "k3s_master_server" {
  
  depends_on = [
    hcloud_network_subnet.k3s_network_subnet,
    random_string.k3s_cluster_token,
  ]


  name        = var.k3s_master_name
  image       = var.k3s_master_image_name
  server_type = var.k3s_master_server_type
  public_net {
    ipv4_enabled = true
    ipv6_enabled = true
  }
  user_data   = templatefile("./configs/k3s-ubuntu-master-cloud-config.yml",
                                {
                                    public_ssh_key = hcloud_ssh_key.ssh_key.public_key,
                                    k3s_linux_user = var.k3s_linux_user,
                                    k3s_channel = var.k3s_channel,
                                    k3s_cluster_token = local.k3_cluster_token,
                                    k3s_private_ip = cidrhost(var.network_subnet, 1),
                                    hcloud_token = var.hcloud_token,
                                    hcloud_network = hcloud_network.k3s_network.id,
                                    cluster_cidr = var.cluster_cidr,
                                }
                            )
}

resource "hcloud_server_network" "k3s_master" {
  
  server_id   = hcloud_server.k3s_master_server.id
  subnet_id   = hcloud_network_subnet.k3s_network_subnet.id
  ip          = cidrhost(var.network_subnet, 1)#TODO: Replace cidrhost to simplify?
}


resource "hcloud_network" "k3s_network" {
  name      = var.network_name
  ip_range  = var.network_range
}

resource "hcloud_network_subnet" "k3s_network_subnet" {
  network_id    = hcloud_network.k3s_network.id
  ip_range      = var.network_subnet
  type          = "server"
  network_zone  = "eu-central"
}

output hcloud_k3s_cluster_token {
  value = local.k3_cluster_token
}