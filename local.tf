locals {
  k3_cluster_token = resource.random_string.k3s_cluster_token.result
}
