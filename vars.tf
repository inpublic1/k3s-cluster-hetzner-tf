
variable "hcloud_token" {
    type        = string
    description = "Hetzner Cloud token"
    default     = ""
}

variable "ssh_key_name" {
    type = string
    description = "SSH Key name to distingueshed in Hetzner cloud account"
    default = "K3S SSH Key"
}

variable "ssh_public_cert" {
    type = string
    description = "SSH Public certificate"
    default = "~/.ssh/id_rsa.pub"
}

variable "ssh_private_cert" {
    type = string
    description = "SSH Private certificate"
    default = "~/.ssh/id_rsa"
}

variable "k3s_linux_user" {
  type = string
  description = "K3S linux user, member of sudoers/admins"
  default = "k3susr"
}

variable "k3s_master_name" {
    type        = string
    description = "K3S cluster master server name."
    default     = "k3s-master"
}

variable "k3s_master_image_name" {
    type        = string
    description = "K3S cluster master server hetzner os image name. Like: ubuntu-22.04"
    default     = "ubuntu-22.04"
}

variable "k3s_master_server_type" {
    type        = string
    description = "K3S luster master Hetzner server type. Like: cx11, cx21, etc."
    default     = "cx11"
}

variable "k3s_channel" {
    default = "stable"
}

variable "cluster_cidr" {
  default = "10.42.0.0/16"
}

variable "network_name" {
  default = "k3s_network"
}

variable "master_firewall_name" {
  default = "k3s_master_firewall"
}

variable "worker_firewall_name" {
  default = "k3s_worker_firewall"
}

variable "network_range" {
  default = "172.16.0.0/12"
}

variable "network_subnet" {
  default = "172.16.1.0/24"
}